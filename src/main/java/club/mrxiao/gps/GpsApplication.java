package club.mrxiao.gps;

import club.mrxiao.gps.common.listener.CloseListener;
import club.mrxiao.gps.common.listener.StartListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author xiaoyu
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
public class GpsApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(GpsApplication.class);
        application.addListeners(new StartListener());
        application.addListeners(new CloseListener());
        application.run(args);
    }
}
