package club.mrxiao.gps.service;

import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.baidu.request.BaiduTraceCommonRequest;
import club.mrxiao.baidu.request.BaiduTraceEntityRequest;
import club.mrxiao.baidu.response.BaiduTraceEntityListResponse;

/**
 * 终端管理服务相关接口
 * @author xiaoyu
 */
public interface EntityManageService {

    /**
     * 创建百度鹰眼终端对象
     * @param baiduTraceEntityRequest
     * @throws BaiduTraceException
     */
    void creationBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException;

    /**
     * 更新百度鹰眼终端对象
     * @param baiduTraceEntityRequest
     * @throws BaiduTraceException
     */
    void updateBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException;

    /**
     * 删除百度鹰眼终端对象
     * @param baiduTraceEntityRequest
     * @throws BaiduTraceException
     */
    void deleteBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException;

    /**
     * 查询百度鹰眼终端列表
     * @param baiduTraceCommonRequest
     * @return
     * @throws BaiduTraceException
     */
    BaiduTraceEntityListResponse findBaiduTraceEntityList(BaiduTraceCommonRequest baiduTraceCommonRequest) throws BaiduTraceException;
}
