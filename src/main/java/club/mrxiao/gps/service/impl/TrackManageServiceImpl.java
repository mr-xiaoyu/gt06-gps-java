package club.mrxiao.gps.service.impl;

import club.mrxiao.baidu.api.BaiduTraceService;
import club.mrxiao.baidu.domain.BaiduTraceTrackPoint;
import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.baidu.response.BaiduTraceTrackAddPointsResponse;
import club.mrxiao.gps.service.TrackManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轨迹点管理相关接口实现
 * @author xiaoyu
 */
@Service
public class TrackManageServiceImpl implements TrackManageService {

    private final BaiduTraceService baiduTraceService;

    @Autowired
    public TrackManageServiceImpl(BaiduTraceService baiduTraceService) {
        this.baiduTraceService = baiduTraceService;
    }

    @Override
    public void baiduTrackAddPoint(BaiduTraceTrackPoint baiduTraceTrackPoint) throws BaiduTraceException {
        this.baiduTraceService.getTrackService().trackAddPoint(baiduTraceTrackPoint);
    }

    @Override
    public BaiduTraceTrackAddPointsResponse baiduTrackAddPoints(List<BaiduTraceTrackPoint> baiduTraceTrackPoints) throws BaiduTraceException {
        return this.baiduTraceService.getTrackService().trackAddPoints(baiduTraceTrackPoints);
    }
}
