package club.mrxiao.gps.common.listener;

import club.mrxiao.gps.common.utils.SpringContextUtil;
import club.mrxiao.gps.socket.GpsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

/**
 * 项目停止监听器
 * @author xiaoyu
 */
@Slf4j
public class CloseListener implements ApplicationListener<ContextClosedEvent> {

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        GpsService service = SpringContextUtil.getBean(GpsService.class);
        service.closeSocketServer();
        log.info("项目停止");
    }
}
