package club.mrxiao.gps.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 项目相关配置
 * @author xiaoyu
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "gps")
public class GpsProperties {

    /**
     * 打包上传频率
     */
    private long uploadFrequency;

    /**
     * socket端口
     */
    private Integer socketPort;
}
