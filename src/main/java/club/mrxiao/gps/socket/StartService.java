package club.mrxiao.gps.socket;

import club.mrxiao.gps.common.properties.GpsProperties;
import club.mrxiao.gps.common.utils.SpringContextUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * socket启动
 * @author xiaoyu
 */
@Component
@Order(value = 1)
public class StartService implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        //配置参数
        GpsProperties gpsProperties = SpringContextUtil.getBean(GpsProperties.class);
        //服务启动类
        GpsService service = SpringContextUtil.getBean(GpsService.class);
        //线程池
        ThreadPoolTaskExecutor gpsExecutor = SpringContextUtil.getBean("gpsExecutor",ThreadPoolTaskExecutor.class);
        service.startSocketServer(gpsProperties.getSocketPort(),gpsExecutor);
    }
}
