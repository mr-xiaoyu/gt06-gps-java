/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : gps_db

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2019-08-27 16:38:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_entity
-- ----------------------------
DROP TABLE IF EXISTS `t_entity`;
CREATE TABLE `t_entity` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `entity_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `entity_imei` varchar(255) DEFAULT NULL COMMENT 'IMEI码',
  `creation_user` int(11) DEFAULT NULL COMMENT '创建人',
  `creation_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_entity
-- ----------------------------
INSERT INTO `t_entity` VALUES ('1', 'test', '0353413532150362', null, '2019-08-26 16:20:57');
